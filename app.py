from flask import Flask 
from flask import render_template
from flask import request

app = Flask(__name__)

@app.route('/')
def hello_world():
	return 'Hello World'

@app.route('/<username>', methods = ['POST'])
def hello_username(username):
	if request.method == 'GET':
		for x in request.args:
			print x + ' ' + str(request.args[x])
		return render_template('hello2.html', name=username)
	else:
		for x in request.form:
			print x + ' ' + str(request.form[x])
		return render_template('hello.html', name=username)

@app.route('/about')
def about_page():
	return 'This is aout us'

if __name__ == '__main__':
	app.run()

