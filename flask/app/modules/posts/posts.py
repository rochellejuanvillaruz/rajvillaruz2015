from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('posts', __name__)

@mod.route('/')
def post_list():
    posts = g.postsdb.getPosts(session['username'])
    return render_template('posts/post.html', posts=posts, date='October 20 2015')

@mod.route('/', methods=['POST'])
def create_post():
    new_post = request.form['new_post']
    emoticon = request.form['emoticon']
    g.postsdb.createPost(new_post, session['username'], emoticon)
    flash('New post created!', 'create_post_success')
    return redirect(url_for('.post_list'))

@mod.route('/', methods=['POST'])
def edit_post():
    edit_post = request.form['edit_post']
    emoticon = request.form['edit_feeling']
    g.postsdb.editPost(edit_post, session['username'], edit_feeling)
    flash('Post is edited!', 'edit_post_success')
    return redirect(url_for('.post_list'))

 