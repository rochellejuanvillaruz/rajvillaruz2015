from flask import Flask
from flask import g

from modules import default
from modules import users
from modules import shopping_list

from pymongo import MongoClient

app = Flask(__name__)

def get_main_db():
    client = MongoClient('mongodb://localhost:27017/')
    maindb = client.postsdb
    return maindb

@app.before_request
def before_request():
    mainDb = get_main_db()
    g.usersdb = users.UserDB(conn=mainDb.users)
    g.inventorydb = shopping_list.shopIDB(conn=mainDb.inventory)
    g.cartdb = shopping_list.shopCDB(conn=mainDb.cart)
    g.tempdb = shopping_list.shopTempDB(conn=mainDb.temp)
    
@app.teardown_request
def teardown_request(exception):
    # inventorydb = getattr(g, 'inventorydb', None)
    # if inventorydb is not None:
    #     inventorydb.close()
    # cartdb = getattr(g, 'cartdb', None)
    # if cartdb is not None:
    #     cartdb.close()
    userdb = getattr(g, 'userdb', None)
    if userdb is not None:
        userdb.close()

app.register_blueprint(default.mod)