from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session
import shop_db

shopmod = Blueprint('shop', __name__)

#@mod.route('/shop')
#def shop_list():
    

@shopmod.route('/add_to_inventory', methods=['POST'])
def add_item_inventory():
    new_item = request.form['item_name']
    item_price = request.form['item_price']
    g.inventorydb.addItemsInventory(new_item, session['username'], item_price)
    g.tempdb.addItemsTemp(new_item, session['username'], item_price)
    #flash('New post created!', 'create_post_success')
    return redirect(url_for('.shoppinglist'))

@shopmod.route('/add_to_cart', methods=['GET'])
def add_item_cart():
    #for item in tempList:
        #g.cartdb.addItemsCart(item['name'], session['username'], item['price'])
    #flash('New post created!', 'create_post_success')
    g.tempdb.removeAll()
    return redirect(url_for('.shop_list'))

@shopmod.route('/add_to_temp', methods=['POST'])
def add_item_cart():
    temp_item = request.form['item_name']
    temp_price = request.form['item_price']
    g.tempdb.addItemsTemp(temp_item, session['username'], temp_price)
    #flash('New post created!', 'create_post_success')
    return redirect(url_for('.shop_list'))