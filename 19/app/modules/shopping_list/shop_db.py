class shopIDB:
	def __init__(self, conn):
		self.conn = conn

	def getItemsInventory(self, username):
		return self.conn.find({'username': username})

	def addItemsInventory(self, item_name, username, item_price, quantity, total):
		self.conn.insert({'name':item_name, 'username': username, 'price': item_price})

class shopCDB:
	def __init__(self, conn):
		self.conn = conn

	def getItemsCart(self, username):
		return self.conn.find({'username': username})

	def addItemsCart(self, item_name, username, item_price):
		self.conn.insert({'name':item_name, 'username': username, 'price': item_price})

class shopTempDB:
	def __init__(self, conn):
		self.conn = conn

	def getItemsTemp(self, username):
		return self.conn.find({'username': username})

	def addItemsTemp(self, item_name, username, item_price):
		self.conn.insert({'name':item_name, 'username': username, 'price': item_price})

	def removeAll(self):
		self.conn.remove({})